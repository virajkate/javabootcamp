// String                                 StringBuffer                            StringBuilder
//  
// 1] String are a immutable                  it is a mutable.                        it is mutable .
// 
// 2] String are a scp.                       StringBuffer are a                 
//    heap memory.                            Storage on heap.                        it is Storage on
//                                                                                     Storage on heap.
// 3] String are more                         StringBuffer are                        
//    memory.                                 less memory                            it is less memory.
//
// 4] it is not Synchronized.                 it is a Synchronized                it is not Syncronized. 
class A{

         public static void main(String[]args){

                     StringBuffer r = new StringBuffer("learn coding");
                  
                     System.out.println(r.reverse());

	           
	            }
	    }
