// Deque.

import java.util.*;
class DequeDemo {

           public static void main(String[]args){


                       Deque obj = new ArrayDeque();

		         obj.offer(10);
		         obj.offer(40);
		         obj.offer(20);
		         obj.offer(30);

                      
		        System.out.println(obj); 

		        // offer first(); 
		        // offerlast();
		       
		          obj.offerFirst(5);
		          obj.offerLast(50);


			  System.out.println(obj);

		            // pollfirst 
			    // polllast

			     obj.pollFirst();
			     obj.pollLast();


			     System.out.println(obj);

		    }


              }
