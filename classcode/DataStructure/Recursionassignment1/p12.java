// Write a program to print the sum of number in the given digit.
import java.util.Scanner;
class RecursionDemo6 {

	               int sumdigit(int num){
                     
			          if(num/10==0){

                                      return num;

					   } 
				    
				     return sumdigit(num/10)+(num%10);

                                       }			         
                       
                       public static void main(String[]args){
                        
                          Scanner sc   =new Scanner(System.in);
			  System.out.println("Enter the number");
			  int n = sc.nextInt();
			  RecursionDemo6  obj = new RecursionDemo6();
			  int  sum = obj.sumdigit(n);                       
			  System.out.println(sum);

			      }	    
                 }
