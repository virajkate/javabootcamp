import java.util.*;
class TreeMapDemo{

      public static void main(String[]args){

       	       SortedMap tm = new TreeMap();

	       tm.put("Ind","India");
	       tm.put("Pak","Pakistan");
	       tm.put("SL","Srilanka");
	       tm.put("Aus","Australia");
	       tm.put("Ban","Bangladesh");
                        
                   System.out.println(tm);// AUS  Ban Ind Pak SL
		   // submap 
		   System.out.println(tm.subMap("AUS","PAK"));
		   // headmap
		   System.out.println(tm.headMap("Pak"));
		   // firstkey
		   System.out.println(tm.firstKey());
		   // Lastkey
		   System.out.println(tm.lastKey());
                   // keyset
		   System.out.println(tm.keySet());
		   //values
		   System.out.println(tm.values());
		  // EntrySet
		  System.out.println(tm.entrySet()); 
	             }
               }
          // Map is not part of the collections in java it is the seperate Interface In java.
