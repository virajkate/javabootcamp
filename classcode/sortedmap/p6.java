// methods of a hashmap.
import java.util.*;

class Demo {

        
       	public static void main(String[]args){
 
                  HashMap hm = new HashMap();

		  hm.put("Java",".java");
		  hm.put("Python",".py");
		  hm.put("Dart",".dart");


		  System.out.println(hm);
		   
	        //get.
	        System.out.println(hm.get("python"));

	       // keyset.
               System.out.println(hm.keySet());

               // Values.
               System.out.println(hm.values());	       
               
              // Entryset.
               System.out.println(hm.entrySet());	

	     }

        }
